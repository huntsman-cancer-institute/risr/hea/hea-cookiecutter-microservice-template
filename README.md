# HEA Cookiecutter Python Microservice Template

The [hea-cookiecutter-microservice-template](https://gitlab.com/huntsman-cancer-institute/risr/hea/hea-cookiecutter-microservice-template)
project provides a template for you to create new microservices rapidly. It will build a project with the above
structure. To use it:
* Install Cookiecutter version 1.7 into your system Python (version 3.5 or higher required) with
`pip install -U cookiecutter~=1.7.3`. Follow any prompts to add the installation directory to your path.
* In your terminal, run `cookiecutter https://gitlab.com/huntsman-cancer-institute/risr/hea/hea-cookiecutter-template`
* Respond to the prompts as follows:
  * *email*: contact email address for the project.
  * *project_name*: replace "Something" in the default with a capitalized name for your new microservice.
  * *project_slug*: the project slug in your version control system. It should start with `heaserver-`. The remainder
    of the slug should be all lowercase, and use dashes between words. If the name refers to a HEA object that the
    microservice will manage,
  * *project_short_description*: a brief description, one phrase, first letter not capitalized, no period.
  * *heaserver_package_name*: the package name in the heaserver namespace, singular and lowercase, that is unique to your
    microservice. The package name, like all package names in Python, should be all lowercase and singular, with no
    dashes or underscores.
  * *heaobject_module_name*: replace with the name of the module in the heaobject project that will contain classes
    managed by your new microservice. The module name, like all module names in python, should be all lowercase and
    singular, with no dashes or underscores.
  * *heaobject_class_name*: the name of the primary HEAObject class managed by your new microservice. The class name,
  like all class names in Python, should be in CamelCase.
* Create a virtual environment for your project, in the project's root directory, with `python -m venv venv`. Activate
  the environment with `source ./venv/bin/activate` on Linux and Mac, and `.\venv\Scripts\activate` on Windows.
* Run `pip install wheel`.
* Run `pip install -r requirements_dev.txt` to install dependencies. Do NOT run `python setup.py install` - the project
  will not work, and you might have to blow away your virtual environment to fix it.
* Run `pytest` and `pytest integrationtests` to see if everything worked (you'll need to create your module in
  the heaobject project first). The integration tests require Docker as a dependency.
