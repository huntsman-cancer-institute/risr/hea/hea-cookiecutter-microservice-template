#!/bin/sh
set -e

cat > .hea-config.cfg <<EOF
[DEFAULT]
Registry=http://heaserver-registry:8080

[MongoDB]
ConnectionString=mongodb://${MONGO_HEA_USERNAME}:${MONGO_HEA_PASSWORD}@${MONGO_HOSTNAME}:27017/${MONGO_HEA_DATABASE}?authSource=${MONGO_HEA_AUTH_SOURCE:-admin}
EOF

exec {{ cookiecutter.project_slug }} -f .hea-config.cfg -b ${HEASERVER_{{ cookiecutter.heaserver_package_name | upper }}_URL}


