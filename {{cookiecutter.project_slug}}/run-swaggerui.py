#!/usr/bin/env python3

from heaserver.{{ cookiecutter.heaserver_package_name }} import service
from heaserver.service import swaggerui
from integrationtests.heaserver.{{ cookiecutter.heaserver_package_name }}integrationtest.testcase import db_store
import logging

logging.basicConfig(level=logging.DEBUG)

if __name__ == '__main__':
    swaggerui.run('{{ cookiecutter.project_slug }}', db_store, service, [('/{{ cookiecutter.heaobject_class_name | lower }}s/{id}', service.get_{{ cookiecutter.heaobject_class_name | lower }})])
