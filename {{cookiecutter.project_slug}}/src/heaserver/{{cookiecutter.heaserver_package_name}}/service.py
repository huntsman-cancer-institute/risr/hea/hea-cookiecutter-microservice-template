"""
The {{ cookiecutter.project_name }} provides ...
"""

from heaserver.service import response, appproperty
from heaserver.service.runner import init_cmd_line, routes, start, web
from heaserver.service.db import mongo, mongoservicelib
from heaserver.service.wstl import builder_factory, action
from heaobject.{{ cookiecutter.heaobject_module_name }} import {{ cookiecutter.heaobject_class_name | capitalize }}

MONGODB_{{ cookiecutter.heaobject_class_name | upper }}_COLLECTION = '{{ cookiecutter.heaobject_class_name | lower }}s'


@routes.get('/{{ cookiecutter.heaobject_class_name | lower }}s/{id}')
@action('{{ cookiecutter.project_slug }}-{{ cookiecutter.heaobject_class_name | lower }}-get-properties', rel='properties')
@action('{{ cookiecutter.project_slug }}-{{ cookiecutter.heaobject_class_name | lower }}-open', rel='opener', path='/{{ cookiecutter.heaobject_class_name | lower }}s/{id}/opener')
@action('{{ cookiecutter.project_slug }}-{{ cookiecutter.heaobject_class_name | lower }}-duplicate', rel='duplicator', path='/{{ cookiecutter.heaobject_class_name | lower }}s/{id}/duplicator')
async def get_{{ cookiecutter.heaobject_class_name | lower }}(request: web.Request) -> web.Response:
    """
    Gets the {{ cookiecutter.heaobject_class_name | lower }} with the specified id.
    :param request: the HTTP request.
    :return: the requested {{ cookiecutter.heaobject_class_name | lower }} or Not Found.
    ---
    summary: A specific {{ cookiecutter.heaobject_class_name | lower }}.
    tags:
        - {{ cookiecutter.heaobject_class_name | lower }}s
    parameters:
        - name: id
          in: path
          required: true
          description: The id of the {{ cookiecutter.heaobject_class_name | lower }} to retrieve.
          schema:
            type: string
    responses:
      '200':
        description: Expected response to a valid request.
        content:
            application/json:
                schema:
                    type: array
                    items:
                        type: object
            application/vnd.collection+json:
                schema:
                    type: array
                    items:
                        type: object
            application/vnd.wstl+json:
                schema:
                    type: array
                    items:
                        type: object
      '404':
        $ref: '#/components/responses/404'
    """
    return await mongoservicelib.get(request, MONGODB_{{ cookiecutter.heaobject_class_name | upper }}_COLLECTION)


@routes.get('/{{ cookiecutter.heaobject_class_name | lower }}s/byname/{name}')
async def get_{{ cookiecutter.heaobject_class_name | lower }}_by_name(request: web.Request) -> web.Response:
    """
    Gets the {{ cookiecutter.heaobject_class_name | lower }} with the specified name.
    :param request: the HTTP request.
    :return: the requested {{ cookiecutter.heaobject_class_name | lower }} or Not Found.
    """
    return await mongoservicelib.get_by_name(request, MONGODB_{{ cookiecutter.heaobject_class_name | upper }}_COLLECTION)


@routes.get('/{{ cookiecutter.heaobject_class_name | lower }}s')
@routes.get('/{{ cookiecutter.heaobject_class_name | lower }}s/')
@action('{{ cookiecutter.project_slug }}-{{ cookiecutter.heaobject_class_name | lower }}-get-properties', rel='properties')
@action('{{ cookiecutter.project_slug }}-{{ cookiecutter.heaobject_class_name | lower }}-open', rel='opener', path='/{{ cookiecutter.heaobject_class_name | lower }}s/{id}/opener')
@action('{{ cookiecutter.project_slug }}-{{ cookiecutter.heaobject_class_name | lower }}-duplicate', rel='duplicator', path='/{{ cookiecutter.heaobject_class_name | lower }}s/{id}/duplicator')
async def get_all_{{ cookiecutter.heaobject_class_name | lower }}s(request: web.Request) -> web.Response:
    """
    Gets all {{ cookiecutter.heaobject_class_name | lower }}s.
    :param request: the HTTP request.
    :return: all {{ cookiecutter.heaobject_class_name | lower }}s.
    """
    return await mongoservicelib.get_all(request, MONGODB_{{ cookiecutter.heaobject_class_name | upper }}_COLLECTION)


@routes.get('/{{ cookiecutter.heaobject_class_name | lower }}s/{id}/duplicator')
@action(name='{{ cookiecutter.project_slug }}-{{ cookiecutter.heaobject_class_name | lower }}-duplicate-form')
async def get_{{ cookiecutter.heaobject_class_name | lower }}_duplicate_form(request: web.Request) -> web.Response:
    """
    Gets a form template for duplicating the requested {{ cookiecutter.heaobject_class_name | lower }}.

    :param request: the HTTP request. Required.
    :return: the requested form, or Not Found if the requested {{ cookiecutter.heaobject_class_name | lower }} was not found.
    """
    return await mongoservicelib.get(request, MONGODB_{{ cookiecutter.heaobject_class_name | upper }}_COLLECTION)


@routes.post('/{{ cookiecutter.heaobject_class_name | lower }}/duplicator')
async def post_{{ cookiecutter.heaobject_class_name | lower }}_duplicator(request: web.Request) -> web.Response:
    """
    Posts the provided {{ cookiecutter.heaobject_class_name | lower }} for duplication.
    :param request: the HTTP request.
    :return: a Response object with a status of Created and the object's URI in the Location header.
    """
    return await mongoservicelib.post(request, MONGODB_{{ cookiecutter.heaobject_class_name | upper }}_COLLECTION, {{ cookiecutter.heaobject_class_name }})


@routes.post('/{{ cookiecutter.heaobject_class_name | lower }}s')
@routes.post('/{{ cookiecutter.heaobject_class_name | lower }}s/')
async def post_{{ cookiecutter.heaobject_class_name | lower }}(request: web.Request) -> web.Response:
    """
    Posts the provided {{ cookiecutter.heaobject_class_name | lower }}.
    :param request: the HTTP request.
    :return: a Response object with a status of Created and the object's URI in the
    """
    return await mongoservicelib.post(request, MONGODB_{{ cookiecutter.heaobject_class_name | upper }}_COLLECTION, {{ cookiecutter.heaobject_class_name | capitalize }})


@routes.put('/{{ cookiecutter.heaobject_class_name | lower }}s/{id}')
async def put_{{ cookiecutter.heaobject_class_name | lower }}(request: web.Request) -> web.Response:
    """
    Updates the {{ cookiecutter.heaobject_class_name | lower }} with the specified id.
    :param request: the HTTP request.
    :return: a Response object with a status of No Content or Not Found.
    """
    return await mongoservicelib.put(request, MONGODB_{{ cookiecutter.heaobject_class_name | upper }}_COLLECTION, {{ cookiecutter.heaobject_class_name | capitalize }})


@routes.delete('/{{ cookiecutter.heaobject_class_name | lower }}s/{id}')
async def delete_{{ cookiecutter.heaobject_class_name | lower }}(request: web.Request) -> web.Response:
    """
    Deletes the {{ cookiecutter.heaobject_class_name | lower }} with the specified id.
    :param request: the HTTP request.
    :return: No Content or Not Found.
    """
    return await mongoservicelib.delete(request, MONGODB_{{ cookiecutter.heaobject_class_name | upper }}_COLLECTION)


def main() -> None:
    config = init_cmd_line(description='{{ cookiecutter.project_short_description }}',
                           default_port=8080)
    start(db=mongo.Mongo, wstl_builder_factory=builder_factory(__package__), config=config)
